<?php
/**
 * TOP API: taobao.tbk.dg.vegas.lbtlj.create request
 * 
 * @author auto create
 * @since 1.0, 2021.11.18
 */
class TbkDgVegasLbtljCreateRequest
{
	/** 
	 * 裂变任务领取截止时间
	 **/
	private $acceptEndTime;
	
	/** 
	 * 裂变任务领取开始时间
	 **/
	private $acceptStartTime;
	
	/** 
	 * 妈妈广告位Id
	 **/
	private $adzoneId;
	
	/** 
	 * CPS佣金类型
	 **/
	private $campaignType;
	
	/** 
	 * 裂变淘礼金邀请人数
	 **/
	private $inviteNum;
	
	/** 
	 * 裂变淘礼金邀请时长；单位：分钟；最大120分钟
	 **/
	private $inviteTimeLimit;
	
	/** 
	 * 宝贝ID
	 **/
	private $itemId;
	
	/** 
	 * 淘礼金名称，最大10个字符
	 **/
	private $name;
	
	/** 
	 * 淘礼金总个数
	 **/
	private $rightsNum;
	
	/** 
	 * 单个淘礼金面额，支持两位小数，单位元
	 **/
	private $rightsPerFace;
	
	/** 
	 * 安全等级，0：适用于常规淘礼金投放场景；1：更高安全级别，适用于淘礼金面额偏大等安全性较高的淘礼金投放场景，可能导致更多用户拦截。security_switch为true，此字段不填写时，使用0作为默认安全级别。如果security_switch为false，不进行安全判断。
	 **/
	private $securityLevel;
	
	/** 
	 * 安全开关，若不进行安全校验，可能放大您的资损风险，请谨慎选择
	 **/
	private $securitySwitch;
	
	/** 
	 * 裂变淘礼金总个数
	 **/
	private $taskRightsNum;
	
	/** 
	 * 裂变单个淘礼金面额，支持两位小数，单位元
	 **/
	private $taskRightsPerFace;
	
	/** 
	 * 使用结束日期。如果是结束时间模式为相对时间，时间格式为1-7直接的整数, 例如，1（相对领取时间1天）； 如果是绝对时间，格式为yyyy-MM-dd，例如，2019-01-29，表示到2019-01-29 23:59:59结束
	 **/
	private $useEndTime;
	
	/** 
	 * 结束日期的模式,1:相对时间，2:绝对时间
	 **/
	private $useEndTimeMode;
	
	/** 
	 * 使用开始日期。相对时间，无需填写，以用户领取时间作为使用开始时间。绝对时间，格式 yyyy-MM-dd，例如，2019-01-29，表示从2019-01-29 00:00:00 开始
	 **/
	private $useStartTime;
	
	/** 
	 * 单用户累计中奖次数上限
	 **/
	private $userTotalWinNumLimit;
	
	private $apiParas = array();
	
	public function setAcceptEndTime($acceptEndTime)
	{
		$this->acceptEndTime = $acceptEndTime;
		$this->apiParas["accept_end_time"] = $acceptEndTime;
	}

	public function getAcceptEndTime()
	{
		return $this->acceptEndTime;
	}

	public function setAcceptStartTime($acceptStartTime)
	{
		$this->acceptStartTime = $acceptStartTime;
		$this->apiParas["accept_start_time"] = $acceptStartTime;
	}

	public function getAcceptStartTime()
	{
		return $this->acceptStartTime;
	}

	public function setAdzoneId($adzoneId)
	{
		$this->adzoneId = $adzoneId;
		$this->apiParas["adzone_id"] = $adzoneId;
	}

	public function getAdzoneId()
	{
		return $this->adzoneId;
	}

	public function setCampaignType($campaignType)
	{
		$this->campaignType = $campaignType;
		$this->apiParas["campaign_type"] = $campaignType;
	}

	public function getCampaignType()
	{
		return $this->campaignType;
	}

	public function setInviteNum($inviteNum)
	{
		$this->inviteNum = $inviteNum;
		$this->apiParas["invite_num"] = $inviteNum;
	}

	public function getInviteNum()
	{
		return $this->inviteNum;
	}

	public function setInviteTimeLimit($inviteTimeLimit)
	{
		$this->inviteTimeLimit = $inviteTimeLimit;
		$this->apiParas["invite_time_limit"] = $inviteTimeLimit;
	}

	public function getInviteTimeLimit()
	{
		return $this->inviteTimeLimit;
	}

	public function setItemId($itemId)
	{
		$this->itemId = $itemId;
		$this->apiParas["item_id"] = $itemId;
	}

	public function getItemId()
	{
		return $this->itemId;
	}

	public function setName($name)
	{
		$this->name = $name;
		$this->apiParas["name"] = $name;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setRightsNum($rightsNum)
	{
		$this->rightsNum = $rightsNum;
		$this->apiParas["rights_num"] = $rightsNum;
	}

	public function getRightsNum()
	{
		return $this->rightsNum;
	}

	public function setRightsPerFace($rightsPerFace)
	{
		$this->rightsPerFace = $rightsPerFace;
		$this->apiParas["rights_per_face"] = $rightsPerFace;
	}

	public function getRightsPerFace()
	{
		return $this->rightsPerFace;
	}

	public function setSecurityLevel($securityLevel)
	{
		$this->securityLevel = $securityLevel;
		$this->apiParas["security_level"] = $securityLevel;
	}

	public function getSecurityLevel()
	{
		return $this->securityLevel;
	}

	public function setSecuritySwitch($securitySwitch)
	{
		$this->securitySwitch = $securitySwitch;
		$this->apiParas["security_switch"] = $securitySwitch;
	}

	public function getSecuritySwitch()
	{
		return $this->securitySwitch;
	}

	public function setTaskRightsNum($taskRightsNum)
	{
		$this->taskRightsNum = $taskRightsNum;
		$this->apiParas["task_rights_num"] = $taskRightsNum;
	}

	public function getTaskRightsNum()
	{
		return $this->taskRightsNum;
	}

	public function setTaskRightsPerFace($taskRightsPerFace)
	{
		$this->taskRightsPerFace = $taskRightsPerFace;
		$this->apiParas["task_rights_per_face"] = $taskRightsPerFace;
	}

	public function getTaskRightsPerFace()
	{
		return $this->taskRightsPerFace;
	}

	public function setUseEndTime($useEndTime)
	{
		$this->useEndTime = $useEndTime;
		$this->apiParas["use_end_time"] = $useEndTime;
	}

	public function getUseEndTime()
	{
		return $this->useEndTime;
	}

	public function setUseEndTimeMode($useEndTimeMode)
	{
		$this->useEndTimeMode = $useEndTimeMode;
		$this->apiParas["use_end_time_mode"] = $useEndTimeMode;
	}

	public function getUseEndTimeMode()
	{
		return $this->useEndTimeMode;
	}

	public function setUseStartTime($useStartTime)
	{
		$this->useStartTime = $useStartTime;
		$this->apiParas["use_start_time"] = $useStartTime;
	}

	public function getUseStartTime()
	{
		return $this->useStartTime;
	}

	public function setUserTotalWinNumLimit($userTotalWinNumLimit)
	{
		$this->userTotalWinNumLimit = $userTotalWinNumLimit;
		$this->apiParas["user_total_win_num_limit"] = $userTotalWinNumLimit;
	}

	public function getUserTotalWinNumLimit()
	{
		return $this->userTotalWinNumLimit;
	}

	public function getApiMethodName()
	{
		return "taobao.tbk.dg.vegas.lbtlj.create";
	}
	
	public function getApiParas()
	{
		return $this->apiParas;
	}
	
	public function check()
	{
		
		RequestCheckUtil::checkNotNull($this->acceptEndTime,"acceptEndTime");
		RequestCheckUtil::checkNotNull($this->acceptStartTime,"acceptStartTime");
		RequestCheckUtil::checkNotNull($this->adzoneId,"adzoneId");
		RequestCheckUtil::checkNotNull($this->inviteNum,"inviteNum");
		RequestCheckUtil::checkNotNull($this->inviteTimeLimit,"inviteTimeLimit");
		RequestCheckUtil::checkNotNull($this->itemId,"itemId");
		RequestCheckUtil::checkNotNull($this->rightsNum,"rightsNum");
		RequestCheckUtil::checkNotNull($this->rightsPerFace,"rightsPerFace");
		RequestCheckUtil::checkNotNull($this->taskRightsNum,"taskRightsNum");
		RequestCheckUtil::checkNotNull($this->taskRightsPerFace,"taskRightsPerFace");
		RequestCheckUtil::checkNotNull($this->useEndTime,"useEndTime");
		RequestCheckUtil::checkNotNull($this->useEndTimeMode,"useEndTimeMode");
	}
	
	public function putOtherTextParam($key, $value) {
		$this->apiParas[$key] = $value;
		$this->$key = $value;
	}
}
