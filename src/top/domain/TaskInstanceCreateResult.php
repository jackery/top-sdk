<?php

/**
 * model
 * @author auto create
 */
class TaskInstanceCreateResult
{
	
	/** 
	 * 直领淘礼金id
	 **/
	public $rights_id;
	
	/** 
	 * 发放url
	 **/
	public $send_url;
	
	/** 
	 * 裂变任务id
	 **/
	public $task_id;
	
	/** 
	 * 裂变淘礼金id
	 **/
	public $task_rights_id;	
}
?>